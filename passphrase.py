import Crypto
import binascii
from Crypto.Hash import SHA
from Crypto.Hash import HMAC

def f(password, ssid, iterations, count):

    input_string=ssid
    input_string+=chr((count>>24)&0xFF)
    input_string+=chr((count>>16)&0xFF)
    input_string+=chr((count>>8)&0xFF)
    input_string+=chr(count&0xFF)
  
    digest = HMAC.new(password,input_string,Crypto.Hash.SHA).hexdigest()
    output=list(digest)
    for i in range(1,iterations):
        digest = HMAC.new(password,binascii.unhexlify(digest),Crypto.Hash.SHA).hexdigest()
        for j in range(0,len(digest)):
            output[j]=hex(int(digest[j],16)^int(output[j],16)).split('x')[1]
    return ''.join(i for i in output)


def passwordHash(password, ssid):
    if len(password) > 63 or len(ssid) > 32:
        return 0;
    Ftot=f(password, ssid, 4096, 1)+f(password, ssid, 4096, 2)
    return Ftot[0:64]


############ STANDARD C IMPLEMENTATION ############

# /*
# * F(P, S, c, i) = U1 xor U2 xor ... Uc
# * U1 = PRF(P, S || Int(i))
# * U2 = PRF(P, U1)
# * Uc = PRF(P, Uc-1)
# */

# void F(
#         char *password,
#         unsigned char *ssid,
#         int ssidlength,
#         int iterations,
#         int count,
#         unsigned char *output)
# {
#     unsigned char digest[36], digest1[A_SHA_DIGEST_LEN];
#     int i, j;
#     for (i = 0; i < strlen(password); i++) {
#         assert((password[i] >= 32) && (password[i] <= 126));
#     } 
#     /* U1 = PRF(P, S || int(i)) */
#     memcpy(digest, ssid, ssidlength);
#     digest[ssidlength] = (unsigned char)((count>>24) & 0xff);
#     digest[ssidlength+1] = (unsigned char)((count>>16) & 0xff);
#     digest[ssidlength+2] = (unsigned char)((count>>8) & 0xff);
#     digest[ssidlength+3] = (unsigned char)(count & 0xff);
#     hmac_sha1(digest, ssidlength+4, (unsigned char*) password,
#             (int) strlen(password), digest, digest1);

#     /* output = U1 */
#     memcpy(output, digest1, A_SHA_DIGEST_LEN);
#     for (i = 1; i < iterations; i++) {
#         /* Un = PRF(P, Un-1) */
#         hmac_sha1(digest1, A_SHA_DIGEST_LEN, (unsigned char*) password, (int) strlen(password), digest);
#         memcpy(digest1, digest, A_SHA_DIGEST_LEN);
#         /* output = output xor Un */
#         for (j = 0; j < A_SHA_DIGEST_LEN; j++) {
#              output[j] ^= digest[j];
#         }
#     }
# }
# /*
#  * password - ascii string up to 63 characters in length
#  * ssid - octet string up to 32 octets
#  * ssidlength - length of ssid in octets
#  * output must be 40 octets in length and outputs 256 bits of key
#  */
# int PasswordHash (
#     char *password,
#     unsigned char *ssid,
#     int ssidlength,
#     unsigned char *output)
# {
#     if ((strlen(password) > 63) || (ssidlength > 32))
#         return 0;
#     F(password, ssid, ssidlength, 4096, 1, output);
#     F(password, ssid, ssidlength, 4096, 2,
#     &output[A_SHA_DIGEST_LEN]);
#     return 1;
# }