#/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import struct

def ROL32( A, n ):
    return (((A) << (n)) | (((A)>>(32-(n))) & ((1 << (n)) - 1 ))) & ((1<<32)-1)

def ROR32(A, n):
    return ROL32( (A), 32-(n) )

def get_unaligned_le32(ptr):
    return ((ptr << 24) & 0xFF000000) | ((ptr<< 8) & 0x00FF0000) | ((ptr >> 8) & 0x0000FF00) | ((ptr >> 24) & 0x000000FF)

def get_unaligned_le16(ptr):
    return ((ptr << 8) & 0xFF00) | ((ptr >> 8) & 0x00FF)

def michael_mic_hdr(mctx, key, priority, DA, SA):
    mctx[0] = get_unaligned_le32(int(key.encode('hex')[0:8],16))
    mctx[1] = get_unaligned_le32(int(key.encode('hex')[8:16],16))

    michael_block(mctx, get_unaligned_le32(int(DA[:8],16)))#
    michael_block(mctx, get_unaligned_le16(int(DA[8:],16)) | ((get_unaligned_le16(int(SA[:4],16))) << 16))
    michael_block(mctx, get_unaligned_le32(int(SA[4:],16)))
    michael_block(mctx, int(priority,16))


def michael_mic(key, data, DA, SA, priority):
    mctx = [0,0]
    michael_mic_hdr(mctx, key, priority, DA, SA)

    blocks = len(data) / 4
    left = len(data) % 4

    for block in range(0,blocks):
        michael_block(mctx, get_unaligned_le32(int(data[block * 4: block * 4 + 4].encode('hex'),16)))

    val = 0x5a
    while (left > 0):
        val <<= 8
        left -= 1
        val |= int(data[blocks * 4 + left].encode('hex'),16)
    
    michael_block(mctx, val)
    michael_block(mctx, 0)

    mic = (get_unaligned_le32(mctx[0]) << 32) | get_unaligned_le32(mctx[1])

    return mic

def michael_mic2(key, data):
    mctx = [0,0]
    michael_mic_hdr2(mctx, key)

    blocks = len(data) / 4
    left = len(data) % 4

    for block in range(0,blocks):
        michael_block(mctx, get_unaligned_le32(int(data[block * 4: block * 4 + 4].encode('hex'),16)))

    val = 0x5a
    while (left > 0):
        val <<= 8
        left -= 1
        val |= int(data[blocks * 4 + left].encode('hex'),16)
    
    michael_block(mctx, val)
    michael_block(mctx, 0)

    mic = (get_unaligned_le32(mctx[0]) << 32) | get_unaligned_le32(mctx[1])

    return mic

def michael_block(mctx, val):
    mctx[0] ^= val
    mctx[1] ^= ROL32(mctx[0], 17)
    mctx[0] += mctx[1]
    mctx[1] ^= ((mctx[0] & 0xff00ff00) >> 8) | ((mctx[0] & 0x00ff00ff) << 8)
    mctx[0] += mctx[1]
    mctx[1] ^= ROL32(mctx[0], 3)
    mctx[0] += mctx[1]
    mctx[1] ^= ROR32(mctx[0], 2)
    mctx[0] += mctx[1]

    mctx[0] &= 0xffffffff


############ ADAPTED FROM ############


 #  1 /*
 #  2  * Michael MIC implementation - optimized for TKIP MIC operations
 #  3  * Copyright 2002-2003, Instant802 Networks, Inc.
 #  4  *
 #  5  * This program is free software; you can redistribute it and/or modify
 #  6  * it under the terms of the GNU General Public License version 2 as
 #  7  * published by the Free Software Foundation.
 #  8  */
 #  9 #include <linux/types.h>
 # 10 #include <linux/bitops.h>
 # 11 #include <linux/ieee80211.h>
 # 12 #include <asm/unaligned.h>
 # 13 
 # 14 #include "michael.h"
 # 15 
 # 16 static void michael_block(struct michael_mic_ctx *mctx, u32 val)
 # 17 {
 # 18         mctx->l ^= val;
 # 19         mctx->r ^= rol32(mctx->l, 17);
 # 20         mctx->l += mctx->r;
 # 21         mctx->r ^= ((mctx->l & 0xff00ff00) >> 8) |
 # 22                    ((mctx->l & 0x00ff00ff) << 8);
 # 23         mctx->l += mctx->r;
 # 24         mctx->r ^= rol32(mctx->l, 3);
 # 25         mctx->l += mctx->r;
 # 26         mctx->r ^= ror32(mctx->l, 2);
 # 27         mctx->l += mctx->r;
 # 28 }
 # 29 
 # 30 static void michael_mic_hdr(struct michael_mic_ctx *mctx, const u8 *key,
 # 31                             struct ieee80211_hdr *hdr)
 # 32 {
 # 33         u8 *da, *sa, tid;
 # 34 
 # 35         da = ieee80211_get_DA(hdr);
 # 36         sa = ieee80211_get_SA(hdr);
 # 37         if (ieee80211_is_data_qos(hdr->frame_control))
 # 38                 tid = *ieee80211_get_qos_ctl(hdr) & IEEE80211_QOS_CTL_TID_MASK;
 # 39         else
 # 40                 tid = 0;
 # 41 
 # 42         mctx->l = get_unaligned_le32(key);
 # 43         mctx->r = get_unaligned_le32(key + 4);
 # 44 
 # 45         /*
 # 46          * A pseudo header (DA, SA, Priority, 0, 0, 0) is used in Michael MIC
 # 47          * calculation, but it is _not_ transmitted
 # 48          */
 # 49         michael_block(mctx, get_unaligned_le32(da));
 # 50         michael_block(mctx, get_unaligned_le16(&da[4]) |
 # 51                             (get_unaligned_le16(sa) << 16));
 # 52         michael_block(mctx, get_unaligned_le32(&sa[2]));
 # 53         michael_block(mctx, tid);
 # 54 }
 # 55 
 # 56 void michael_mic(const u8 *key, struct ieee80211_hdr *hdr,
 # 57                  const u8 *data, size_t data_len, u8 *mic)
 # 58 {
 # 59         u32 val;
 # 60         size_t block, blocks, left;
 # 61         struct michael_mic_ctx mctx;
 # 62 
 # 63         michael_mic_hdr(&mctx, key, hdr);
 # 64 
 # 65         /* Real data */
 # 66         blocks = data_len / 4;
 # 67         left = data_len % 4;
 # 68 
 # 69         for (block = 0; block < blocks; block++)
 # 70                 michael_block(&mctx, get_unaligned_le32(&data[block * 4]));
 # 71 
 # 72         /* Partial block of 0..3 bytes and padding: 0x5a + 4..7 zeros to make
 # 73          * total length a multiple of 4. */
 # 74         val = 0x5a;
 # 75         while (left > 0) {
 # 76                 val <<= 8;
 # 77                 left--;
 # 78                 val |= data[blocks * 4 + left];
 # 79         }
 # 80 
 # 81         michael_block(&mctx, val);
 # 82         michael_block(&mctx, 0);
 # 83 
 # 84         put_unaligned_le32(mctx.l, mic);
 # 85         put_unaligned_le32(mctx.r, mic + 4);
 # 86 }
 # 87 
