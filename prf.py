import Crypto
from Crypto.Hash import SHA
from Crypto.Hash import HMAC


def PRF(key,prefix,data,output_len):
    input_string=prefix+chr(0)+data
    out = ""
    for i in range(0,(output_len+19)/20):
        out += HMAC.new(key,input_string+chr(i),Crypto.Hash.SHA).hexdigest()
    return out[0:(2*output_len)]


if __name__ == '__main__':
	print "hello"

############ STANDARD C IMPLEMENTATION ############

#  /*
#        * PRF -- Length of output is in octets rather than bits
#        *     since length is always a multiple of 8 output array is
#        *     organized so first N octets starting from 0 contains PRF output
#        *
#        *     supported inputs are 16, 24, 32, 48, 64
#        *     output array must be 80 octets to allow for sha1 overflow
#        */
#       void PRF(
#           unsigned char *key, int key_len,
#           unsigned char *prefix, int prefix_len,
#           unsigned char *data, int data_len,
#           unsigned char *output, int len)
# {
# int i;
#           unsigned char input[1024]; /* concatenated input */
#           int currentindex = 0;
#           int total_len;
#           memcpy(input, prefix, prefix_len);
#           input[prefix_len] = 0; /* single octet 0 */
#           memcpy(&input[prefix_len+1], data, data_len);
#           total_len = prefix_len + 1 + data_len;
#           input[total_len] = 0; /* single octet count, starts at 0 */
#           total_len++;
#           for (i = 0; i < (len+19)/20; i++) {
#               hmac_sha1(input, total_len, key, key_len,
#                  &output[currentindex]);
#               currentindex += 20;/* next concatenation location */
#               input[total_len-1]++; /* increment octet count */
#           }
# }