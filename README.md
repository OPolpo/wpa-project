#Installation

The program need *Scapy* and *pycrypto* modules.
To install it run:

	pip install scapy


Pip should be included in Python from 2.7.9 If you don't have pip please refer to: [How to Install Pip](https://pip.pypa.io/en/latest/installing.html)

#Execution

To launch the program:

	python wpa.py path_to_captured_pcap.pcap "SSID" "Password" [LIMIT]
	
##Arguments
There is not any command line parser, so you must respect the given order of the arguments.

- The first path_to_captured_pcap.pcap is the pcap file encrypted that you want to decrypt.
- The second one is the SSID
- The Third one is the Password
- And the last is the limit that is how many packets you want to analyse.

(For example if you have a huge pcap and you want to consider only the fist 1000 packet you could set the LIMIT to 1000)
	
	python wpa.py path_to_captured_pcap.pcap "SSID" "Password" 1000
	
##Output

For each packet the program print:

	packet ###

If the Packet have the flag protected set to 1 (i.e. is encrypted) the program try to decrypt it with the derived key. And print the Michael MIC, the ICV, the encrypted payload (It prints the wepdata so I think that the first few bits are missing), and the decrypted paylod (without the given Michael and the ICV)

	packet 498
	    ICV: 6b13d659 Computed ICV: 6b13d659 OK
	    Michael 95644bcff5fcfebf Computed Michael 95644bcff5fcfebf OK
	    wepdata:
	000000002bc27bc6f54b25d152099a51de2671cccc77b233fb010a3ccb9d0e05fe430e8ef8edc8b063b0912df54d46456fd1808ffea45c30a5b66157949262efc5ba3fdf253ce6f6068d65bf
	    decriptet payload:
	aaaa03000000080045000034dcd940003b06b25f17ddd6a3c0a801620050e24da9ba706647438635801001c53f9400000101080ac81cacc43b000ac0

If the packet analysed is and Eapol Key (type 2 or 3) the program prints
	packet ###
	    EAPOL #
##Sample

If you put the given pcap on desktop **(dummypassword_OmniNet.pcap)** you must use as **SSID: "OmniNet"** and as **Password: "dummypassword"** and you could launch the program with:

	python wpa.py ~/Desktop/dummypassword_OmniNet.pcap "OmniNet" "dummypassword"

