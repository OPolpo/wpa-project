#!/usr/bin/env python

import sys 
import binascii
import ctypes
import prf
import passphrase
import phase
import michael
import Crypto
from scapy.all import *
from scapy.utils import rdpcap
from Crypto.Cipher import ARC4
from scapy.utils import PcapWriter


USAGE = "python wpa.py encrypted.pcap ssid password [packet-limit]"
PTK = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
TK = "0000000000000000000000000000000000000000000000000000000000000000"

def int32_to_uint32(i):
    return ctypes.c_uint32(i).value

def ieee8021x_type(pack):
    return binascii.hexlify(pack[58])

def gimmeThePTK(passwd, ssid, ANonce, SNonce,AA,SPA):
    prefix = "Pairwise key expansion"
    key = passphrase.passwordHash(passwd,ssid)
    # print "psk :"
    # print key
    print "\n"
    data = min(AA, SPA) + max(AA, SPA) + min(ANonce, SNonce) + max(ANonce, SNonce)
    return prf.PRF(key.decode('hex'), prefix, data, 64)

def gimmeTheTK(PTK):
    TK_str = PTK[64:128]
    return [int(TK_str[i:i+2], 16) for i in range(0, len(TK_str), 2)]

def getKCK(PTK):
    return PTK[:32]

def getAUTH_TO_SUPP_MK(PTK):
    return PTK[96:112]

def getSUPP_TO_AUTH_MK(PTK):
    return PTK[112:128]

def getIV32(v64):
    return v64 & 0xFFFFFFFF
    
def getIV16(v64):
    return ((v64 >> 56) & 0xFF)+((v64 >> 40) & 0xFF)

def extractdecryptionINFO(eapol_key_packet_2, eapol_key_packet_3):
    #i don't care about 1st and 4th packet, cause the aim of the project is just derive the same key i just use the last 2nd and 3rd packet exchanged
    if (eapol_key_packet_2 == 0
        or eapol_key_packet_3 == 0
        and checkMIC(eapol_key_packet_2,getKCK(PTK))
        and checkMIC(eapol_key_packet_3,getKCK(PTK))): 
        return 0

    AA = eapol_key_packet_3.addr2.replace(":", "")
    SPA = eapol_key_packet_3.addr1.replace(":", "")

    ANonce = eapol_key_packet_2.getlayer(EAPOL).load[13:45].encode('hex') #packet1 e packet3 have the same Nonce
    SNonce = eapol_key_packet_3.getlayer(EAPOL).load[13:45].encode('hex') 

    info = {}
    info["AA"]=AA
    info["SPA"]=SPA
    info["ANonce"]=ANonce
    info["SNonce"]=SNonce

    return info


def decryptWPAPSK(dot11):
    IV = int((dot11.iv+chr(dot11.keyid)+dot11.wepdata[0:4]).encode('hex'),16)
    TA = getSourceAddr(dot11)
    TA_as_int_list = [int(TA[i:i+2],16) for i in range(0, len(TA), 2)]
    RC4key_as_int_list = phase.phase2(TK, phase.phase1(TK, TA_as_int_list, getIV32(IV)), getIV16(IV))
    RC4key = ''.join(("%0.2X" % k) for k in RC4key_as_int_list).decode('hex')
    return ARC4.new(RC4key).decrypt(dot11.wepdata[4:])


def checkMIC(eapol_key, KCK):
    null_mic = "00000000000000000000000000000000".decode('hex')
    key_mic_set = int(str(eapol_key)[5].encode('hex'),16) & 0x01

    if key_mic_set == 0x01 :
        eapol_frame_0 = (str(eapol_key)[:4] + eapol_key.load[:76] + null_mic + eapol_key.load[92:])
        return HMAC.new(KCK.decode('hex'),eapol_frame_0,Crypto.Hash.MD5).digest() == eapol_key.load[77:93]
    else :
        return null_mic == eapol_key.load[77:93]

#todo

def getSourceAddr(dot11):
    to_ds = dot11.FCfield & 0x0001
    from_ds = (dot11.FCfield>>1) & 0x0001

    if(to_ds == 0 and from_ds == 0):
        return dot11.addr2.replace(":", "")
    elif(to_ds == 0 and from_ds == 1):
        return dot11.addr3.replace(":", "")
    elif(to_ds == 1 and from_ds == 0):
        return dot11.addr2.replace(":", "")
    elif(to_ds == 1 and from_ds == 1):
        return dot11.addr4.replace(":", "")


def getDestinationAddr(dot11):
    to_ds = dot11.FCfield & 0x0001
    from_ds = (dot11.FCfield>>1) & 0x0001

    if(to_ds == 0 and from_ds == 0):
        return dot11.addr1.replace(":", "")
    elif(to_ds == 0 and from_ds == 1):
        return dot11.addr1.replace(":", "")
    elif(to_ds == 1 and from_ds == 0):
        return dot11.addr3.replace(":", "")
    elif(to_ds == 1 and from_ds == 1):
        return dot11.addr3.replace(":", "")


def getPriority(dot11):
    return str(dot11.getlayer(Dot11QoS)).encode('hex')[0:2]

def getRightMic(dot11, decrypt_info, PTK):
    if getDestinationAddr(dot11) == decrypt_info["SPA"]:
        return getAUTH_TO_SUPP_MK(PTK).decode('hex')
    elif getDestinationAddr(dot11) == decrypt_info["AA"]:
        return getSUPP_TO_AUTH_MK(PTK).decode('hex')
    elif getSourceAddr(dot11) == decrypt_info["AA"]:
        return getAUTH_TO_SUPP_MK(PTK).decode('hex')
    elif getSourceAddr(dot11) == decrypt_info["SPA"]:
        return getSUPP_TO_AUTH_MK(PTK).decode('hex')
    return "0000000000000000"


def main():
    global TK, PTK
    if len(sys.argv) != 4 and len(sys.argv) != 5:
        print " ".join(sys.argv) +" is non a valid command"
        print "    " + USAGE
        return 0

    # pktdump = PcapWriter("out.pcap", append=False, sync=True)

    ssid = sys.argv [2]
    passwd = sys.argv [3]

    eapol_key_packet_2 = 0
    eapol_key_packet_3 = 0

    decrypt_info = {}
    decrypt_info = 0
    derived_tk = False

    pckt_idx = 0
    packets = rdpcap(sys.argv[1])
    for packet in packets:
        pckt_idx += 1
        print ("packet %d" % pckt_idx)
        if packet.haslayer(EAPOL):
            eapol = packet.getlayer(EAPOL)
            if eapol.type == EAPOL.KEY: #maybe we can verify that we use psk and tkip
                print int(eapol.load[2:3].encode('hex'),16)
                if  (
                        ((int(eapol.load[2:3].encode('hex'),16) & 0x40) == 0x00) and #install
                        ((int(eapol.load[2:3].encode('hex'),16) & 0x80) == 0x00) and #ack
                        ((int(eapol.load[1:2].encode('hex'),16) & 0x01) == 0x01) and #mic
                        (eapol.load[93:95] != "0000".decode('hex')) #if dataleght is different from zero it's 2nd and not 1st 
                    ):
                    eapol_key_packet_2 = packet
                    print "    EAPOL 2"

                elif(
                        ((int(eapol.load[2:3].encode('hex'),16) & 0x40) == 0x40) and #install
                        ((int(eapol.load[2:3].encode('hex'),16) & 0x80) == 0x80) and #ack
                        ((int(eapol.load[1:2].encode('hex'),16) & 0x01) == 0x01) and #mic
                        (eapol.load[93:95] != "0000".decode('hex')) #if dataleght is different from zero it's 3rd and not 4th 
                    ):
                    eapol_key_packet_3=packet
                    print "    EAPOL 3"
                    decrypt_info = extractdecryptionINFO(eapol_key_packet_2, eapol_key_packet_3)

                if decrypt_info != 0 and derived_tk == False: # we have derived the key
                    PTK = gimmeThePTK(passwd,ssid,decrypt_info["ANonce"].decode('hex'),decrypt_info["SNonce"].decode('hex'),decrypt_info["AA"].decode('hex'),decrypt_info["SPA"].decode('hex'))
                    TK = gimmeTheTK(PTK)
                    derived_tk = True
                    print "    TK Derived"


        if derived_tk == True:
            if packet.haslayer(Dot11):
                dot11 = packet.getlayer(Dot11)
                # print dot11.FCfield
                if dot11.FCfield & 0x40 == 0x40:#check protected flag

                    cleartext_mic_icv = decryptWPAPSK(dot11)
                    cleartext_mic = cleartext_mic_icv[:-4]
                    cleartext = cleartext_mic_icv[:-12]
                    icv = cleartext_mic_icv[-4:]
                    michael_mic = cleartext_mic_icv[-12:-4]


                    icv_copmuted = hex(int32_to_uint32(binascii.crc32(cleartext_mic_icv[:-4])))[2:-1]
                    # print icv_copmuted
                    icv_computed_formatted = "".join(reversed([icv_copmuted[i:i+2] for i in range(0, len(icv_copmuted), 2)])).ljust(8,'0')

                    crc32_fail = False
                    if(icv_computed_formatted != icv.encode('hex')):
                        crc32_fail = True

                    print "    ICV: %s Computed ICV: %s %s" % (icv.encode('hex'), icv_computed_formatted, ("CONTROLLO FALLITO" if crc32_fail else "OK"))
                    # print "key " + getRightMic(dot11, decrypt_info, PTK).encode('hex')
                    # print "DA " + getDestinationAddr(dot11)
                    # print "SA " + getSourceAddr(dot11)
                    # print "PR " + getPriority(dot11)
                    michael_mic_computed = hex(michael.michael_mic(getRightMic(dot11, decrypt_info, PTK), cleartext, getDestinationAddr(dot11), getSourceAddr(dot11), getPriority(dot11)))[2:-1]
                    
                    michael_mic_fail = False
                    if(michael_mic_computed != michael_mic.encode('hex')):
                        michael_mic_fail = True
                    
                    print "    Michael %s Computed Michael %s %s" % (michael_mic.encode('hex'), michael_mic_computed, ("CONTROLLO FALLITO" if michael_mic_fail else "OK"))

                    if(not michael_mic_fail and not crc32_fail):
                        print "    wepdata:"
                        print dot11.wepdata.encode('hex');
                        print "    decriptet payload:"
                        print cleartext.encode('hex')

        if len(sys.argv) == 5:
            if pckt_idx > int(sys.argv[4]):
                return

        # pktdump.write(packet)



if __name__ == '__main__':
  main()


